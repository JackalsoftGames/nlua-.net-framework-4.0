using System;

namespace NLua.Event
{
    public class HookExceptionEventArgs : EventArgs
    {
        public Exception Exception { get; set; }

        public HookExceptionEventArgs(Exception ex)
        {
            Exception = ex;
        }
    }
}